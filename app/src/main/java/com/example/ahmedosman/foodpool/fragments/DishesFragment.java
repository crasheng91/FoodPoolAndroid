package com.example.ahmedosman.foodpool.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ahmedosman.foodpool.R;
import com.example.ahmedosman.foodpool.adapters.DishesRecyclerViewAdapter;
import com.example.ahmedosman.foodpool.internet.ApiGenerateService;
import com.example.ahmedosman.foodpool.models.Dish;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DishesFragment extends Fragment implements DishesRecyclerViewAdapter.RecyclerViewClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String kitchen_id;
    private String api_token;

    private OnDishesInteraction mListener;
    private RecyclerView mRecyclerView;
    private ArrayList<Dish> dishesList;
    private DishesRecyclerViewAdapter mDishesRecyclerViewAdapter;
    private com.wang.avi.AVLoadingIndicatorView loadingAvi;

    public DishesFragment() {
        // Required empty public constructor
    }


    public static DishesFragment newInstance(String param1, String param2) {
        DishesFragment fragment = new DishesFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            kitchen_id = getArguments().getString(ARG_PARAM1);
            api_token = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_dishes, container, false);

        mRecyclerView = view.findViewById(R.id.dishesRecyclerViewId);

        dishesList = new ArrayList<>();

        mDishesRecyclerViewAdapter = new DishesRecyclerViewAdapter(dishesList, this, getActivity());
        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(lm);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mDishesRecyclerViewAdapter);

        loadingAvi = view.findViewById(R.id.dish_avi);


        //Ask internet for restaurants
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + api_token);

        new ApiGenerateService().getAPI().getDishes(headers, Integer.valueOf(kitchen_id)).enqueue(new Callback<List<Dish>>() {
            @Override
            public void onResponse(Call<List<Dish>> call, Response<List<Dish>> response) {
                if (response.code() == 200){
                    mDishesRecyclerViewAdapter.notifyDataChangeCustom(response.body());
                    Toast.makeText(getActivity(), "All dishes", Toast.LENGTH_SHORT).show();
                    loadingAvi.hide();
                }
            }

            @Override
            public void onFailure(Call<List<Dish>> call, Throwable t) {

            }
        });

//        //Loading animation
//        loadingAvi.show();
//        //init list of restaurants

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onDishFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnDishesInteraction) {
            mListener = (OnDishesInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnDishesInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void recyclerViewButtonClicked(View v, int position, String dishId, double price, String name, float rating, String image) {
        //Open Dialog to get the qty
        FragmentManager fm = getFragmentManager();

        AddToCartFragment addToCartFragment = AddToCartFragment.newInstance(price,dishId, name, rating, image);
        addToCartFragment.show(fm, "Add Items Number");
    }


    public interface OnDishesInteraction {
        // TODO: Update argument type and name
        void onDishFragmentInteraction(Uri uri);
    }
}
