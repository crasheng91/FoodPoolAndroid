package com.example.ahmedosman.foodpool.helpers;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by ahmedosman on 12/31/17.
 */

public class LoginSharedPreferences {

    SharedPreferences.Editor editor;
    SharedPreferences settings;
    private Context context;
    private String phoneNumber, password, accessToken, Email, Name;
    private Integer id;


    public LoginSharedPreferences(Context context) {
        this.context = context;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
    }


    public String getPhoneNumber() {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("phoneNumber", "");
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("phoneNumber", this.phoneNumber);
        editor.commit();
    }

//    public void setShopID(String shopId) {
//        this.shopID = shopId;
//        settings = context.getSharedPreferences("login", 0);
//        editor = settings.edit();
//        editor.putString("shopID", this.shopID);
//        editor.commit();
//    }
//
//    public String getShopID() {
//        settings = context.getSharedPreferences("login", 0);
//        return settings.getString("shopID", "");
//    }

    public String getPassword() {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("password", "");
    }

    public void setPassword(String password) {
        this.password = password;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("password", this.password);
        editor.commit();
    }

    public String getAccessToken() {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("accessToken", "");
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("accessToken", accessToken);
        editor.commit();

    }


    public int getId() {
        settings = context.getSharedPreferences("login", 0);
        return settings.getInt("id", 0);
    }

    public void setId(Integer id) {
        this.id = id;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putInt("id", id);
        editor.commit();

    }


    public String getEmail() {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("email", "");
    }

    public void setEmail(String email) {
        this.Email = email;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("email", email);
        editor.commit();

    }


    public String getName() {
        settings = context.getSharedPreferences("login", 0);
        return settings.getString("Name", "");
    }

    public void setName(String Name) {
        this.Name = Name;
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.putString("Name", Name);
        editor.commit();

    }

    public void removeLogin(Context context) {
        settings = context.getSharedPreferences("login", 0);
        editor = settings.edit();
        editor.remove("accessToken");
        editor.remove("phoneNumber");
        editor.remove("password");
        editor.remove("email");
        editor.commit();
    }
}
