package com.example.ahmedosman.foodpool.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.ahmedosman.foodpool.R;
import com.example.ahmedosman.foodpool.adapters.OrderRecyclerViewAdapter;
import com.example.ahmedosman.foodpool.helpers.LoginSharedPreferences;
import com.example.ahmedosman.foodpool.internet.ApiGenerateService;
import com.example.ahmedosman.foodpool.models.Order;
import com.example.ahmedosman.foodpool.models.OrderRequest;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.realm.Realm;
import io.realm.RealmResults;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnCartFragmentInteraction} interface
 * to handle interaction events.
 * Use the {@link CartFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CartFragment extends Fragment implements OrderRecyclerViewAdapter.DeleteOneOfOrdersHasBeenClicked, View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnCartFragmentInteraction mListener;
    private Realm realm;
    private OrderRecyclerViewAdapter mAdapter;
    private android.support.v7.widget.RecyclerView recyclerView;
    private Button mButtomOrderNow;

    public CartFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment CartFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CartFragment newInstance(String param1, String param2) {
        CartFragment fragment = new CartFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cart, container, false);

        realm = Realm.getDefaultInstance();
        recyclerView =  view.findViewById(R.id.orderRecyclerViewId);
        setUpRecyclerView();

        mButtomOrderNow = view.findViewById(R.id.button_order_all);
        mButtomOrderNow.setOnClickListener(this);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onCartFragInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnCartFragmentInteraction) {
            mListener = (OnCartFragmentInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnCartFragmentInteraction");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    private void setUpRecyclerView() {
        mAdapter = new OrderRecyclerViewAdapter(getActivity(), realm.where(Order.class).findAll(), this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.setAdapter(mAdapter);
        recyclerView.setHasFixedSize(true);

//        TouchHelperCallback touchHelperCallback = new TouchHelperCallback();
//        ItemTouchHelper touchHelper = new ItemTouchHelper(touchHelperCallback);
//        touchHelper.attachToRecyclerView(recyclerView);
    }

    @Override
    public void DeleteOneOfOrdersHasBeenClicked(View v, int position, int dishId) {
        realm.beginTransaction();
        RealmResults<Order> orders = realm.where(Order.class)
                .equalTo("dish_id", dishId)
                .findAll();
        orders.deleteAllFromRealm();
        realm.commitTransaction();
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.button_order_all:
                sendAllRequests();
                break;
            default:
                //do nothing
        }

    }

    private void sendAllRequests() {

        LoginSharedPreferences loginSharedPreferences = new LoginSharedPreferences(getActivity());
        String api_token = loginSharedPreferences.getAccessToken();

        realm = Realm.getDefaultInstance();
        RealmResults<Order> orders = realm.where(Order.class).findAll();

        if (orders == null || orders.size() == 0){
            Toast.makeText(getActivity(), "You have no orders", Toast.LENGTH_SHORT).show();
            return;
        }
        List<OrderRequest> all_request_dishes = new ArrayList<>();
        for (int i = 0; i < orders.size(); i++){
            OrderRequest userdish  = new OrderRequest();
            userdish.setDishId(orders.get(i).getDish_id());
            userdish.setQty(orders.get(i).getQty());
            userdish.setSubTotal(orders.get(i).getSub_total());
            all_request_dishes.add(userdish);
        }


        //Ask internet for restaurants
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + api_token);
        headers.put("Content-Type", "application/json");

        new ApiGenerateService().getAPI().postOrders(headers, all_request_dishes).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.code() == 200){
                    Toast.makeText(getContext(), "Ordered", Toast.LENGTH_SHORT).show();
                    realm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {
                            RealmResults<Order> result = realm.where(Order.class).findAll();
                            result.deleteAllFromRealm();
                        }
                    });
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }
        });
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnCartFragmentInteraction {
        // TODO: Update argument type and name
        void onCartFragInteraction(Uri uri);
    }

}
