package com.example.ahmedosman.foodpool;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.ahmedosman.foodpool.fragments.SignInFragment;
import com.example.ahmedosman.foodpool.fragments.SignUpFragment;
import com.example.ahmedosman.foodpool.helpers.LoginSharedPreferences;

public class CredentialsActivity extends AppCompatActivity implements SignUpFragment.OnSignUpFragmentInteractionListener, SignInFragment.OnSignInInteraction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credentials);

        // Check that the activity is using the layout version with
        // the fragment_container FrameLayout
        LoginSharedPreferences loginSharedPreferences = new LoginSharedPreferences(getBaseContext());
        if (loginSharedPreferences.getAccessToken() != ""){
            Intent intent = new Intent(CredentialsActivity.this, LandActivity.class);
            Bundle bundle = new Bundle();
            bundle.putString("token", loginSharedPreferences.getAccessToken());
            bundle.putString("username", loginSharedPreferences.getName());
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }
        if (findViewById(R.id.fragmentHolderId) != null) {

            // However, if we're being restored from a previous state,
            // then we don't need to do anything and should return or else
            // we could end up with overlapping fragments.
            if (savedInstanceState != null) {
                return;
            }

            // Create a new Fragment to be placed in the activity layout
            SignUpFragment signUpFragment = new SignUpFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            signUpFragment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragmentHolderId, signUpFragment).commit();
        }
    }

    @Override
    public void onSignUpInteraction(String token, String username) {

        if (token == null){
            // Create a new Fragment to be placed in the activity layout
            SignInFragment signInFraqgment = new SignInFragment();

            // In case this activity was started with special instructions from an
            // Intent, pass the Intent's extras to the fragment as arguments
            signInFraqgment.setArguments(getIntent().getExtras());

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.fragmentHolderId, signInFraqgment).addToBackStack(null).commit();
        }else{
            Bundle bundle = new Bundle();
            bundle.putString("token", token);
            bundle.putString("username", username);

            Intent intent = new Intent(this, LandActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
            finish();
        }
    }

    @Override
    public void onSignInInteraction(Uri uri) {
        Bundle bundle = new Bundle();
        LoginSharedPreferences loginSharedPreferences = new LoginSharedPreferences(getBaseContext());
        String token = loginSharedPreferences.getAccessToken();
        String userName = loginSharedPreferences.getName();
        bundle.putString("token", token);
        bundle.putString("username", userName);

        Intent intent = new Intent(this, LandActivity.class);
        intent.putExtras(bundle);
        startActivity(intent);
        finish();
    }
}
