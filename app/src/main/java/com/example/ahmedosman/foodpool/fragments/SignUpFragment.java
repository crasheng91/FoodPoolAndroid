package com.example.ahmedosman.foodpool.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.ahmedosman.foodpool.R;
import com.example.ahmedosman.foodpool.helpers.LoginSharedPreferences;
import com.example.ahmedosman.foodpool.internet.ApiGenerateService;
import com.example.ahmedosman.foodpool.models.RegisterationResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class SignUpFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnSignUpFragmentInteractionListener mListener;
    private Button mSignInButton;
    private Button mSignUpButtom;
    private TextInputLayout mTextInputName;
    private TextInputLayout mTextInputEmail;
    private TextInputLayout mTextInputPassword;
    private TextInputLayout mTextInputPasswordConfirmation;

    public SignUpFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignUpFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SignUpFragment newInstance(String param1, String param2) {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_sign_up, container, false);

        //View inflation
        mSignInButton = view.findViewById(R.id.signInButtonId);
        mSignUpButtom = view.findViewById(R.id.signUpButtonId);
        mTextInputName = view.findViewById(R.id.name);
        mTextInputEmail = view.findViewById(R.id.email);
        mTextInputPassword = view.findViewById(R.id.password);
        mTextInputPasswordConfirmation = view.findViewById(R.id.passwordConfirmation);


        //hooking an event
        mSignInButton.setOnClickListener(this);
        mSignUpButtom.setOnClickListener(this);


        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String input, String input2) {
        if (mListener != null) {
            mListener.onSignUpInteraction(input, input2);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSignUpFragmentInteractionListener) {
            mListener = (OnSignUpFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSignUpFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.signInButtonId:
                onButtonPressed(null, null);
                break;
            case R.id.signUpButtonId:
                if(validInputs())
                Toast.makeText(getActivity(), "process request", Toast.LENGTH_SHORT).show();
                    signUpNewUser();
                break;

            default:
                //Do nothing
        }
    }

    private void signUpNewUser() {
        String name = mTextInputName.getEditText().getText().toString();
        String email = mTextInputEmail.getEditText().getText().toString();
        String password = mTextInputPassword.getEditText().getText().toString();
        String passwordConfirm = mTextInputPasswordConfirmation.getEditText().getText().toString();

        //prevent submit two times
        mSignUpButtom.setEnabled(false);

        new ApiGenerateService().getAPI().postRegister(name, email, password, passwordConfirm, false).enqueue(new Callback<RegisterationResponse>() {
            @Override
            public void onResponse(Call<RegisterationResponse> call, Response<RegisterationResponse> response) {
                if (response.code() == 201){
                    //Record created user allowed in
                    EventBus.getDefault().post(new MessageSignUpEvent(response.body()));
                }else{
                    EventBus.getDefault().post(new MessageSignUpEvent(null));
                }

            }

            @Override
            public void onFailure(Call<RegisterationResponse> call, Throwable t) {
                Log.d("network", "onFailure: " +  t.toString());

            }
        });
    }

    private boolean validInputs() {

        //if no input applied
        if (mTextInputName.getEditText().getText() == null
                || mTextInputEmail.getEditText().getText() == null
                || mTextInputPassword.getEditText().getText() == null
                || mTextInputPasswordConfirmation.getEditText().getText() == null){
            Toast.makeText(getActivity(), "Please provide input", Toast.LENGTH_SHORT).show();
            return false;
        }


        //if no input applied
        if (mTextInputName.getEditText().getText().toString() == ""
                || mTextInputEmail.getEditText().getText().toString() == ""
                || mTextInputPassword.getEditText().getText().toString() == ""
                || mTextInputPasswordConfirmation.getEditText().getText().toString() == ""){
            Toast.makeText(getActivity(), "Please provide valid input", Toast.LENGTH_SHORT).show();
            return false;
        }

        //Check if the password fields are same
        if (!mTextInputPassword.getEditText().getText().toString()
                .equals(mTextInputPasswordConfirmation.getEditText().getText().toString())){
            Toast.makeText(getActivity(), "Your password fields not matched", Toast.LENGTH_SHORT).show();
        }

        return true;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageSignUpEvent event) {
        if (event.message == null){
            Toast.makeText(getActivity(), "check your info", Toast.LENGTH_SHORT).show();
            return;
        }
        String api_token = event.message.getData().getApiToken();
        Integer id = event.message.getData().getId();
        String name = event.message.getData().getName();
        String email = event.message.getData().getEmail();

        LoginSharedPreferences loginSharedPreferences = new LoginSharedPreferences(getActivity());
        loginSharedPreferences.setAccessToken(api_token);
        loginSharedPreferences.setName(name);
        loginSharedPreferences.setEmail(email);
        loginSharedPreferences.setId(id);

        Toast.makeText(getActivity(), "Registered Successully", Toast.LENGTH_SHORT).show();

        //Enable the button again
        mSignUpButtom.setEnabled(true);

        onButtonPressed(api_token, name);

        //TODO
        //Hide Loading
        //Go to the land activity
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSignUpFragmentInteractionListener {
        // TODO: Update argument type and name
        void onSignUpInteraction(String input, String input2);
    }

    public static class MessageSignUpEvent {
        public final RegisterationResponse message;

        public MessageSignUpEvent(RegisterationResponse message) {
            this.message = message;
        }

    }

}
