package com.example.ahmedosman.foodpool.internet;

import com.example.ahmedosman.foodpool.models.Dish;
import com.example.ahmedosman.foodpool.models.LoginResponse;
import com.example.ahmedosman.foodpool.models.OrderRequest;
import com.example.ahmedosman.foodpool.models.RegisterationResponse;
import com.example.ahmedosman.foodpool.models.Restaurant;

import java.util.List;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;

/**
 * Created by ahmedosman on 12/31/17.
 */

public interface ApiServiceInterface {

    @Headers({"Accept: application/json", "Content-Type: application/x-www-form-urlencoded"})
    @FormUrlEncoded
    @POST("register")
    Call<RegisterationResponse> postRegister(
            @Field("name") String name,
            @Field("email") String email,
            @Field("password") String password,
            @Field("password_confirmation") String password_confirmation,
            @Field("registerAdmin") boolean admin);


    @Headers({"Accept: application/json", "Content-Type: application/x-www-form-urlencoded"})
    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> postLogin(
            @Field("email") String email,
            @Field("password") String password);


    @GET("restaurants")
    Call<List<Restaurant>> getRestaurants(
            @HeaderMap Map<String, String> headers);


    @GET("restaurants/{id}")
    Call<List<Dish>> getDishes(
            @HeaderMap Map<String, String> headers, @Path("id") int id);


    @POST("orders")
    Call<ResponseBody> postOrders(
            @HeaderMap Map<String, String> headers, @Body List<OrderRequest> orders);

}
