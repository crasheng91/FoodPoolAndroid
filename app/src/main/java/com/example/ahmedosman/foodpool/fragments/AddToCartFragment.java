package com.example.ahmedosman.foodpool.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.ahmedosman.foodpool.R;
import com.example.ahmedosman.foodpool.models.Order;

import io.realm.Realm;

public class AddToCartFragment extends DialogFragment implements View.OnClickListener {
    // TODO: Rename parameter arguments, choose names that match
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";

    // TODO: Rename and change types of parameters
    private double itemPrice;
    private String dish_id;
    private Realm mRealm;

//    private OnCartFragmentInteraction mListener;
    private ImageButton mButtonUp;
    private ImageButton mButtonDown;
    private TextView mTextNumberOfItems;
    private Button mButtonAdd;
    private TextView mTextSubTotal;
    private int askedQty=1;
    private String dish_name;
    private float dish_rating;
    private String dish_image;

    public AddToCartFragment() {
        // Required empty public constructor
    }


    public static AddToCartFragment newInstance(double param1, String param2, String name, float rating, String image) {
        AddToCartFragment fragment = new AddToCartFragment();
        Bundle args = new Bundle();
        args.putDouble(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, name);
        args.putFloat(ARG_PARAM4, rating);
        args.putString(ARG_PARAM5, image);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            itemPrice = getArguments().getDouble(ARG_PARAM1);
            dish_id = getArguments().getString(ARG_PARAM2);
            dish_name = getArguments().getString(ARG_PARAM3);
            dish_rating = getArguments().getFloat(ARG_PARAM4);
            dish_image = getArguments().getString(ARG_PARAM5);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_to_cart, container, false);

        mButtonUp = view.findViewById(R.id.increaseId);
        mButtonDown = view.findViewById(R.id.decreaseId);
        mTextNumberOfItems = view.findViewById(R.id.itemNumbersId);
        mTextSubTotal = view.findViewById(R.id.subTotalDishPrice);
        mButtonAdd = view.findViewById(R.id.add_sub_to_cart_id);

        mButtonAdd.setOnClickListener(this);
        mButtonUp.setOnClickListener(this);
        mButtonDown.setOnClickListener(this);

        //Set the item price / item
        mTextSubTotal.setText(String.valueOf(itemPrice) + "  SAR");

        mRealm = Realm.getDefaultInstance();
        return view;
    }

//    // TODO: Rename method, update argument and hook method into UI event
//    public void onButtonPressed(Uri uri) {
//        if (mListener != null) {
//            mListener.onCartFragInteraction(uri);
//        }
//    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
//        if (context instanceof OnCartFragmentInteraction) {
//            mListener = (OnCartFragmentInteraction) context;
//        } else {
//            throw new RuntimeException(context.toString()
//                    + " must implement OnCartFragmentInteraction");
//        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
//        mListener = null;
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.increaseId:
                askedQty = changeTheNumber("plus");
                break;
            case R.id.decreaseId:
                askedQty = changeTheNumber("minus");
                break;
            case R.id.add_sub_to_cart_id:


                //Save to realm database in order table
                //dish id , sub total price, Qty
                Order order = new Order();
                order.setQty(Integer.valueOf(askedQty));
                order.setSub_total(Double.valueOf(askedQty*itemPrice));
                order.setDish_id(Integer.valueOf(dish_id));
                order.setDish_image(dish_image);
                order.setDish_name(dish_name);
                order.setRating(dish_rating);
//                addDataToRealm(order);


                mRealm.beginTransaction();
                final Order entry = mRealm.copyToRealmOrUpdate(order); // Persist unmanaged objects
                mRealm.commitTransaction();
                dismiss();

                break;
            default:
                //Do nothing
        }
    }

    private int changeTheNumber(String type) {
        int currentQty = Integer.valueOf(mTextNumberOfItems.getText().toString());
        if (type.equals("plus")){
            currentQty += 1;
        }else{
            currentQty -= 1;
        }
        //if user goes less than one
        if(currentQty < 1)
            return 1;
        mTextSubTotal.setText((currentQty * itemPrice) + "  SAR");
        mTextNumberOfItems.setText(String.valueOf(currentQty));
        return currentQty;
    }

//
//    public interface OnCartFragmentInteraction {
//        // TODO: Update argument type and name
//        void onCartFragInteraction(Uri uri);
//    }


    private void addDataToRealm(Order model) {
        mRealm.beginTransaction();
        mRealm.createObject(Order.class, model);
//        order.setDish_id(model.getDish_id());
//        order.setQty(model.getQty());
//        order.setSub_total(model.getSub_total());
        mRealm.commitTransaction();
    }
}
