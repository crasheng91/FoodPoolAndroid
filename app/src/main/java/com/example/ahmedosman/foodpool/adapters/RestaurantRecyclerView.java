package com.example.ahmedosman.foodpool.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ahmedosman.foodpool.R;
import com.example.ahmedosman.foodpool.models.Restaurant;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ahmedosman on 1/1/18.
 */

public class RestaurantRecyclerView extends RecyclerView.Adapter<RestaurantRecyclerView.ViewHolder> {


    private static RecyclerViewClickListener itemListener;
    RecyclerViewClickListener mListener;
    private Context context;
    private List<Restaurant> values;
    private int lastPosition = -1;

    public RestaurantRecyclerView(List<Restaurant> data, RecyclerViewClickListener itemListener, Context ctx) {
        this.context = ctx;
        setHasStableIds(true);
        values = data;
        this.itemListener = itemListener;
    }


    @Override
    public RestaurantRecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView inflatedView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_restaurant_recycler_view, parent, false);

        ViewHolder vh = new ViewHolder(inflatedView);
        return vh;
    }

    public void notifyDataChangeCustom(List<Restaurant> restaurants){
        this.values = restaurants;
        this.notifyDataSetChanged();
    }
    @Override
    public void onBindViewHolder(RestaurantRecyclerView.ViewHolder holder, int position) {

        String pictureUrl = "http://104.131.185.15/images/"+values.get(position).getPicture();
        holder.restaurantTitle.setText(values.get(position).getTitle());
        Picasso.with(context)
                .load(pictureUrl)
                .resize(150, 100)
                .centerCrop()
                .into(holder.restaurantImage);
        holder.restaurantRating.setRating(4);

        setAnimation(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
    }

    @Override
    public long getItemId(int position) {
        return super.getItemId(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView restaurantTitle;
        public RatingBar restaurantRating;
        public ImageView restaurantImage;

        public ViewHolder(View itemView) {
            super(itemView);
            restaurantTitle = itemView.findViewById(R.id.restaurant_title_id);
            restaurantRating = itemView.findViewById(R.id.restaurant_rating_id);
            restaurantImage = itemView.findViewById(R.id.restaurant_image_id);
            restaurantRating = itemView.findViewById(R.id.restaurant_rating_id);

            restaurantRating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener() {
                public void onRatingChanged(RatingBar ratingBar, float rating,
                                            boolean fromUser) {

                    Toast.makeText(context, String.valueOf(rating), Toast.LENGTH_SHORT).show();

                }
            });
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            itemListener.recyclerViewListClicked(view, this.getLayoutPosition(),
                    String.valueOf(values.get(this.getLayoutPosition()).getId()), values.get(this.getLayoutPosition()).getTitle());
        }
    }


    public interface RecyclerViewClickListener {

        void recyclerViewListClicked(View v, int position, String RestaurantId, String restaurantName);
    }

    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
