package com.example.ahmedosman.foodpool.internet;

import com.example.ahmedosman.foodpool.models.OrderRequest;
import com.facebook.stetho.okhttp3.StethoInterceptor;

import java.io.IOException;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Protocol;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ahmedosman on 12/31/17.
 */

public class ApiGenerateService {
    ApiServiceInterface api;
    String url = "http://104.131.185.15/api/";

    private static final String TAG = OrderRequest.class.getSimpleName();


    private static OkHttpClient okHttpClient = new OkHttpClient.Builder()
            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .addNetworkInterceptor(new StethoInterceptor())
            .build();

//    OkHttpClient client = new OkHttpClient.Builder()
//            // this is my interceptor to set the authorization header
//            .addInterceptor(new Interceptor() {
//                @Override
//                public Response intercept(Chain chain) throws IOException {
//                    Request request = chain.request()
//                            .newBuilder()
//                            .build();
//                    return chain.proceed(request);
//                }
//            })
//            .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BASIC))
//            .protocols(Arrays.asList(/*Protocol.HTTP_2, */Protocol.HTTP_1_1))
//            .build();


    public ApiGenerateService() {
        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl(url)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        this.api = retrofit.create(ApiServiceInterface.class);
    }


    public ApiServiceInterface getAPI() {
        return this.api;
    }
}
