package com.example.ahmedosman.foodpool.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderRequest {

    @SerializedName("dish_id")
    @Expose
    private Integer dishId;
    @SerializedName("qty")
    @Expose
    private Integer qty;
    @SerializedName("sub_total")
    @Expose
    private Double subTotal;

    public Integer getDishId() {
        return dishId;
    }

    public void setDishId(Integer dishId) {
        this.dishId = dishId;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public Double getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(Double subTotal) {
        this.subTotal = subTotal;
    }

}

