package com.example.ahmedosman.foodpool;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import com.example.ahmedosman.foodpool.fragments.CartFragment;
import com.example.ahmedosman.foodpool.fragments.DishesFragment;
import com.example.ahmedosman.foodpool.fragments.ResturantsFragment;
import com.example.ahmedosman.foodpool.helpers.LoginSharedPreferences;

public class LandActivity extends AppCompatActivity implements ResturantsFragment.OnRestaurantInteractionFragment,
        DishesFragment.OnDishesInteraction, CartFragment.OnCartFragmentInteraction {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_land);



        if (findViewById(R.id.landFragmentHolderId) != null) {

            // However, if we're being restored from a previous state,
            if (savedInstanceState != null) {
                return;
            }
//            getSupportActionBar().hide();

            Intent intent = getIntent();
            String api_token = intent.getStringExtra("token");
            String username = intent.getStringExtra("username");

//            Intent jj = new Intent(LandActivity.this, CartActivity.class);
//            startActivity(jj);
            android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);

//            toolbar.setTitle("Restaurants");



            // Create a new Fragment to be placed in the activity layout
            ResturantsFragment resturantsFragment = ResturantsFragment.newInstance(api_token, null);

            // Add the fragment to the 'fragment_container' FrameLayout
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.landFragmentHolderId, resturantsFragment).commit();

        }
    }


    //communicate from fragment of restaurants to the activity
    // for opening a detailed screen of dishes
    @Override
    public void onRestFragmentInteraction(String restaurantId, String restaurantName) {

//        android.support.v7.widget.Toolbar toolbar = findViewById(R.id.toolbar);
//        toolbar.setTitle(restaurantName);

        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(restaurantName);
        }

        Intent intent = getIntent();
        String api_token = intent.getStringExtra("token");
        DishesFragment newFragment = DishesFragment.newInstance(restaurantId, api_token);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        // Replace whatever is in the fragment_container view with this fragment,
        // and add the transaction to the back stack so the user can navigate back
        transaction.replace(R.id.landFragmentHolderId, newFragment);
        transaction.addToBackStack(null);

        // Commit the transaction
        transaction.commit();
    }


    @Override
    public void onDishFragmentInteraction(Uri uri) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.custom_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_main_shopping:
                FragmentManager fm = getSupportFragmentManager();
                fm.beginTransaction().replace(R.id.landFragmentHolderId, new CartFragment()).addToBackStack(null).commit();
                return true;
            case R.id.menu_main_logout:
                logout();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void logout() {
        LoginSharedPreferences loginSharedPreferences = new LoginSharedPreferences(getApplicationContext());
        loginSharedPreferences.setAccessToken("");
        Intent intent = new Intent(this, CredentialsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onCartFragInteraction(Uri uri) {

    }
}
