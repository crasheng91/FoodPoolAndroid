package com.example.ahmedosman.foodpool.fragments;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.ahmedosman.foodpool.R;
import com.example.ahmedosman.foodpool.helpers.LoginSharedPreferences;
import com.example.ahmedosman.foodpool.internet.ApiGenerateService;
import com.example.ahmedosman.foodpool.models.Data;
import com.example.ahmedosman.foodpool.models.LoginResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link OnSignInInteraction} interface
 * to handle interaction events.
 * Use the {@link SignInFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SignInFragment extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private Button mSignInButton;

    private OnSignInInteraction mListener;
    private TextInputLayout mTextInputEmail;
    private TextInputLayout mTextInputPassword;

    public SignInFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment SignInFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static SignInFragment newInstance(String param1, String param2) {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_sign_in, container, false);
        mSignInButton = view.findViewById(R.id.signInSubmit);
        mTextInputEmail = view.findViewById(R.id.emailSignIn);
        mTextInputPassword = view.findViewById(R.id.passwordSignIn);

        mSignInButton.setOnClickListener(this);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onSignInInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnSignInInteraction) {
            mListener = (OnSignInInteraction) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnSignUpFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


    private void signIn(){
        String email = mTextInputEmail.getEditText().getText().toString();
        String password = mTextInputPassword.getEditText().getText().toString();

        if (email == "" || password == "")
                return;


        new ApiGenerateService().getAPI().postLogin(email, password).enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                loggedIn(response.code(), response.body().getData());
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {

            }
        });
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.signInSubmit:
                signIn();
                break;
            default:
                //Do nothing
        }
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnSignInInteraction {
        // TODO: Update argument type and name
        void onSignInInteraction(Uri uri);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageSignInEvent event) {


        //TODO
        //Hide Loading
        //Go to the land activity
    }

    public static class MessageSignInEvent {
        public final LoginResponse message;

        public MessageSignInEvent(LoginResponse message) {
            this.message = message;
        }

    }

    private void loggedIn(int code, Data response){
        if (code != 200){
            Toast.makeText(getActivity(), "check your info", Toast.LENGTH_SHORT).show();
            return;
        }

//        Toast.makeText(getContext(), response.getData().getApiToken(), Toast.LENGTH_SHORT).show();
        String api_token = response.getApiToken();
        Integer id = response.getId();
        String name = response.getName();
        String email = response.getEmail();

        LoginSharedPreferences loginSharedPreferences = new LoginSharedPreferences(getActivity());
        loginSharedPreferences.setAccessToken(api_token);
        loginSharedPreferences.setName(name);
        loginSharedPreferences.setEmail(email);
        loginSharedPreferences.setId(id);

        Toast.makeText(getActivity(), "Logged In Successfully", Toast.LENGTH_SHORT).show();

        //Enable the button again
        onButtonPressed(null);
    }

}
