package com.example.ahmedosman.foodpool.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.example.ahmedosman.foodpool.CredentialsActivity;
import com.example.ahmedosman.foodpool.R;
import com.example.ahmedosman.foodpool.adapters.RestaurantRecyclerView;
import com.example.ahmedosman.foodpool.helpers.LoginSharedPreferences;
import com.example.ahmedosman.foodpool.internet.ApiGenerateService;
import com.example.ahmedosman.foodpool.models.Restaurant;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class ResturantsFragment extends Fragment implements RestaurantRecyclerView.RecyclerViewClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "api_token";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String api_token;
    private String mParam2;

    private OnRestaurantInteractionFragment mListener;
    private RecyclerView mRecyclerView;
    private RestaurantRecyclerView mRestRecyclerAdapter;
    private List<Restaurant> restaurantsList;
    private com.wang.avi.AVLoadingIndicatorView lodingAvi;

    //    private String api_token;
    public ResturantsFragment() {
        // Required empty public constructor
    }


    public static ResturantsFragment newInstance(String param1, String param2) {
        ResturantsFragment fragment = new ResturantsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            api_token = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_resturants, container, false);

        //inflate recyclerview
        mRecyclerView = view.findViewById(R.id.restaurantRecyclerViewId);

        restaurantsList = new ArrayList<>();


        mRestRecyclerAdapter = new RestaurantRecyclerView(restaurantsList, this, getActivity());
        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        lm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(lm);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setAdapter(mRestRecyclerAdapter);


        //Loading animation
        lodingAvi = view.findViewById(R.id.avi);
        lodingAvi.show();
        //init list of restaurants




        //Ask internet for restaurants
        Map<String, String> headers = new HashMap<>();
        headers.put("Accept", "application/json");
        headers.put("Authorization", "Bearer " + api_token);


        new ApiGenerateService().getAPI().getRestaurants(headers).enqueue(new Callback<List<Restaurant>>() {
            @Override
            public void onResponse(Call<List<Restaurant>> call, Response<List<Restaurant>> response) {
                if (response.code() == 200){
                    EventBus.getDefault().post(new MessageRestaurantsAvailable(response.body(), true));
                }
                if (response.code() == 401){
                    EventBus.getDefault().post(new MessageRestaurantsAvailable(null, false));
                }
            }

            @Override
            public void onFailure(Call<List<Restaurant>> call, Throwable t) {
                EventBus.getDefault().post(new MessageRestaurantsAvailable(null, true));
            }
        });

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(String restaurantId, String restaurantName) {
        if (mListener != null) {
            mListener.onRestFragmentInteraction(restaurantId, restaurantName);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnRestaurantInteractionFragment) {
            mListener = (OnRestaurantInteractionFragment) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnRestaurantInteractionFragment");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(MessageRestaurantsAvailable event) {
        if (event.allowed == false){
            Toast.makeText(getActivity(), "unauthorized", Toast.LENGTH_SHORT).show();
            LoginSharedPreferences loginSharedPreferences = new LoginSharedPreferences(getContext());
            loginSharedPreferences.setAccessToken("");
            Intent intent = new Intent(this.getActivity(), CredentialsActivity.class);
            startActivity(intent);
            getActivity().finish();
            return;
        }
        if (event.message == null)
            Toast.makeText(getActivity(), "Internet Problem", Toast.LENGTH_SHORT).show();
        else{
            restaurantsList = event.message;
            mRestRecyclerAdapter.notifyDataChangeCustom(restaurantsList);
        }

        //Hide it data is available
        lodingAvi.hide();
    }

    @Override
    public void recyclerViewListClicked(View v, int position, String RestaurantId, String restaurantName) {
        onButtonPressed(RestaurantId, restaurantName);
    }



    public interface OnRestaurantInteractionFragment {
        // TODO: Update argument type and name
        void onRestFragmentInteraction(String restaurantId, String restaurantName);
    }

    public static class MessageRestaurantsAvailable {
        public final List<Restaurant> message;

        //if false redirect him to sign up and remove token
        public final boolean allowed;

        public MessageRestaurantsAvailable(List<Restaurant> message, boolean allowed) {
            this.message = message;
            this.allowed = allowed;
        }

    }
}
