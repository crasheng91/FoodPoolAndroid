package com.example.ahmedosman.foodpool.adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.ahmedosman.foodpool.R;
import com.example.ahmedosman.foodpool.models.Dish;
import com.example.ahmedosman.foodpool.models.Restaurant;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by ahmedosman on 1/1/18.
 */

public class DishesRecyclerViewAdapter extends RecyclerView.Adapter<DishesRecyclerViewAdapter.ViewHolder> {

    private static RecyclerViewClickListener itemListener;
    DishesRecyclerViewAdapter.RecyclerViewClickListener mListener;
    private Context context;
    private List<Dish> values;
    private int lastPosition = -1;

    public DishesRecyclerViewAdapter(List<Dish> data, DishesRecyclerViewAdapter.RecyclerViewClickListener itemListener, Context ctx) {
        this.context = ctx;
        setHasStableIds(true);
        values = data;
        this.itemListener = itemListener;
    }

    @Override
    public DishesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView inflatedView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_dish_recycler_view, parent, false);
        DishesRecyclerViewAdapter.ViewHolder vh = new DishesRecyclerViewAdapter.ViewHolder(inflatedView);
        return vh;
    }

    @Override
    public void onBindViewHolder(DishesRecyclerViewAdapter.ViewHolder holder, int position) {
        String pictureUrl = "http://104.131.185.15/images/"+values.get(position).getPicture();
        holder.dishTitle.setText(values.get(position).getTitle());
        holder.dishPrice.setText(String.valueOf(values.get(position).getPrice()) + "  SAR");
        Picasso.with(context)
                .load(pictureUrl)
                .resize(150, 100)
                .centerCrop()
                .into(holder.dishImage);
        holder.dishRating.setRating(4);

        setAnimation(holder.itemView, position);
    }

    public void notifyDataChangeCustom(List<Dish> dishes){
        this.values = dishes;
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return values.size();
    }

    public interface RecyclerViewClickListener {

        void recyclerViewButtonClicked(View v, int position, String  dishId, double price, String name, float rating, String image);
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView dishTitle;
        public RatingBar dishRating;
        public ImageView dishImage;
        public Button dishOrderButton;
        public TextView dishPrice;


        public ViewHolder(View itemView) {
            super(itemView);
            dishTitle = itemView.findViewById(R.id.dish_title_id);
            dishRating = itemView.findViewById(R.id.dish_rating_id);
            dishImage = itemView.findViewById(R.id.dish_image_id);
            dishPrice = itemView.findViewById(R.id.dish_price_id);
            dishOrderButton = itemView.findViewById(R.id.dish_button_order);
            dishOrderButton.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Dish dish = values.get(this.getLayoutPosition());
            itemListener.recyclerViewButtonClicked(view, this.getLayoutPosition(),
                    String.valueOf(dish.getId()),
                    dish.getPrice(),
                    dish.getTitle(),
                    dish.getRating(),
                    dish.getPicture());
        }
    }


    private void setAnimation(View viewToAnimate, int position)
    {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(context, android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}
