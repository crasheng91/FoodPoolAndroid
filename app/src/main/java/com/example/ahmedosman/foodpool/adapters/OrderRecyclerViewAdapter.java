package com.example.ahmedosman.foodpool.adapters;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.example.ahmedosman.foodpool.R;
import com.example.ahmedosman.foodpool.models.Dish;
import com.example.ahmedosman.foodpool.models.Order;
import com.squareup.picasso.Picasso;

import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Created by ahmedosman on 1/2/18.
 */

public class OrderRecyclerViewAdapter extends  RealmRecyclerViewAdapter<Order,OrderRecyclerViewAdapter.ViewHolder > {

    Context mContext;
    OrderedRealmCollection<Order> orders;
    private static DeleteOneOfOrdersHasBeenClicked itemListener;

    public OrderRecyclerViewAdapter(Context context, OrderedRealmCollection<Order> data, DeleteOneOfOrdersHasBeenClicked itemListener) {
        super(data, true);
        // Only set this if the model class has a primary key that is also a integer or long.
        // In that case, {@code getItemId(int)} must also be overridden to return the key.
        // See https://developer.android.com/reference/android/support/v7/widget/RecyclerView.Adapter.html#hasStableIds()
        // See https://developer.android.com/reference/android/support/v7/widget/RecyclerView.Adapter.html#getItemId(int)
        mContext = context;
        orders = data;
        this.itemListener = itemListener;
        setHasStableIds(true);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView inflatedView = (CardView) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item_recycler_view, parent, false);
        OrderRecyclerViewAdapter.ViewHolder vh = new OrderRecyclerViewAdapter.ViewHolder(inflatedView);
        return vh;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Order order = orders.get(position);
        String pictureUrl = "http://104.131.185.15/images/"+ order.getDish_image();
        Picasso.with(mContext)
                .load(pictureUrl)
                .resize(150, 100)
                .centerCrop()
                .into(holder.imageDishOrder);
        holder.titleDishOrder.setText(order.getDish_name());
        holder.priceDishOrder.setText(String.valueOf(order.getSub_total()) + "  SAR");
        holder.ratingDishOrder.setRating(4);
        holder.qtyDishOrder.setText(" Qty : " + String.valueOf(order.getQty()));

    }

    @Override
    public int getItemCount() {
        return orders.size();
    }

    @Nullable
    @Override
    public Order getItem(int index) {
        return orders.get(index);
    }

    @Override
    public long getItemId(int index) {
        //noinspection ConstantConditions
        return getItem(index).getDish_id();
    }


    public interface DeleteOneOfOrdersHasBeenClicked {

        void DeleteOneOfOrdersHasBeenClicked(View v, int position, int  dishId);
    }

    public class ViewHolder extends  RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView titleDishOrder;
        public ImageView imageDishOrder;
        public RatingBar ratingDishOrder;
        public TextView qtyDishOrder;
        public TextView priceDishOrder;
        public ImageButton buttonDeleteDishOrder;

        public ViewHolder(View itemView) {
            super(itemView);
            titleDishOrder = itemView.findViewById(R.id.order_dish_name_id);
            imageDishOrder = itemView.findViewById(R.id.order_image_id);
            ratingDishOrder = itemView.findViewById(R.id.order_dish_rating_id);
            qtyDishOrder = itemView.findViewById(R.id.order_dish_qty_id);
            priceDishOrder = itemView.findViewById(R.id.order_dish_price_id);
            buttonDeleteDishOrder = itemView.findViewById(R.id.dish_button_delete);
            buttonDeleteDishOrder.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Order dish = orders.get(this.getLayoutPosition());
            itemListener.DeleteOneOfOrdersHasBeenClicked(view, this.getLayoutPosition(),
                    dish.getDish_id());
        }
    }
}
